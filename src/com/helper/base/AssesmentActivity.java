package com.helper.base;

import com.helper.base.R;
import com.helper.db.DBHelper;

import android.os.Bundle;
import android.app.Activity;
import android.database.Cursor;
import android.graphics.Typeface;
import android.util.TypedValue;
import android.view.Menu;
import android.view.View;
import android.widget.*;

public class AssesmentActivity extends Activity {

	DBHelper dbh;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//setContentView(R.layout.activity_assesment);
		
		initComponents();
	}

	private void initComponents() {
		DBHelper dbh = new DBHelper(getApplicationContext());
		
		Cursor c = dbh.selectAll();
		
		TableLayout table = new TableLayout(this);  
	    table.setStretchAllColumns(true);  
	    table.setShrinkAllColumns(true);
	    
	    if (c.moveToFirst())
	    	do {
	    		TableRow rowTerm = new TableRow(this);
	    		TableRow rowConcept = new TableRow(this);
//	    		String category = c.getString(0);
	    		String term = c.getString(1);
	    		String concept = c.getString(2);
	    		
	    		// -- Category --
//	    		textView.setText(category);
//	    	    row.addView(textView);
	    	    
	    	    // -- Term --
	    	    TextView termView = new TextView(this);
	    	    termView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 18);
	    	    termView.setTypeface(Typeface.SERIF, Typeface.BOLD); 
	    	    termView.setPadding(10, 10, 0, 10);
	    	    termView.setText(term);
	    	    
	    	    // -- Concept --
	    	    final TextView conceptView = new TextView(this);
	    	    conceptView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 16);
//	    	    conceptView.setTypeface(Typeface.SERIF, Typeface.BOLD);
	    	    conceptView.setText(concept);
	    	    conceptView.setPadding(20, 10, 0, 10);
	    	    conceptView.setVisibility(View.GONE);
	    	    
	    	    termView.setOnClickListener(new View.OnClickListener() {
	                @Override
	                public void onClick(View v) {
	                    // TODO Auto-generated method stub
	                	if (conceptView.getVisibility() == View.GONE)
	                		conceptView.setVisibility(View.VISIBLE);
	                	else
	                		conceptView.setVisibility(View.GONE);
	                }
	            });
	    	    
	    	    rowTerm.addView(termView);
	    	    rowConcept.addView(conceptView);
	    		table.addView(rowTerm);
	    		table.addView(rowConcept);
	    	} while (c.moveToNext());
	    
	    setContentView(table);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_assesment, menu);
		return true;
	}

}
