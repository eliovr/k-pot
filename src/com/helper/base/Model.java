package com.helper.base;

import java.sql.Date;
import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.helper.common.*;
import com.helper.db.DBHelper;

public class Model implements IModel{

	private DBHelper dbHelper;
	
	public Model(Context context){
		this.dbHelper = new DBHelper(context);
	}
	
	@Override
	public void insert(Course course) {
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		ContentValues initialValues = new ContentValues();
		
		initialValues = new ContentValues();
		initialValues.put(DBHelper.TABLE_COURSE_COL_DESCRIPTION, course.getDescription());
		initialValues.put(DBHelper.TABLE_COURSE_COL_DATE, course.getDate().getTime());
		
		db.insert(DBHelper.TABLE_COURSE_NAME, null, initialValues);
	}

	@Override
	public void insert(Category category) {
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		ContentValues initialValues = new ContentValues();
		int id = 0;
		
		Cursor cursor = db.query( DBHelper.TABLE_CATEGORY_NAME, 
				new String[] { DBHelper.TABLE_CATEGORY_COL_ID }, 
				DBHelper.TABLE_CATEGORY_COL_DESCRIPTION + " = ? AND " + DBHelper.TABLE_CATEGORY_COL_COURSE + " = ? ", 
				new String [] { category.getDescription(), String.valueOf(category.getCourse().getId()) }, 
				null, null, null );
		
		if (cursor.moveToFirst() ){
			id = cursor.getInt(0);
		}
		else{
			initialValues = new ContentValues();
			initialValues.put(DBHelper.TABLE_CATEGORY_COL_DESCRIPTION, category.getDescription());
			initialValues.put(DBHelper.TABLE_CATEGORY_COL_COURSE, category.getCourse().getId());
			initialValues.put(DBHelper.TABLE_CATEGORY_COL_DATE, category.getDate().getTime());
			
			id = (int)db.insert(DBHelper.TABLE_CATEGORY_NAME, null, initialValues);
		}
		
		category.setId(id);
	}
	
	@Override
	public void insert(Term term){
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		ContentValues initialValues = new ContentValues();
		int id = 0;
		
		Cursor cursor = db.query( DBHelper.TABLE_TERM_NAME, 
				new String[] { DBHelper.TABLE_TERM_COL_ID }, 
				DBHelper.TABLE_TERM_COL_DESCRIPTION + " = ? AND " + DBHelper.TABLE_TERM_COL_CATEGORY + " = ?", 
				new String [] { term.getDescription(), String.valueOf(term.getCategory().getId()) }, 
				null, null, null );
		
		if (cursor.moveToFirst() ){
			id = cursor.getInt(0);
		}
		else{
			initialValues.put(DBHelper.TABLE_TERM_COL_DESCRIPTION, term.getDescription());
			initialValues.put(DBHelper.TABLE_TERM_COL_CATEGORY, term.getCategory().getId());
			initialValues.put(DBHelper.TABLE_TERM_COL_DATE, term.getDate().getTime());
			
			id = (int)db.insert(DBHelper.TABLE_TERM_NAME, null, initialValues);
		}
		
		term.setId(id);
	}
	
	@Override
	public void insert(Concept concept) {
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		ContentValues initialValues = new ContentValues();
		int id = 0;
		
		// -- insert the "concept"
		initialValues = new ContentValues();
		initialValues.put(DBHelper.TABLE_CONCEPT_COL_DESCRIPTION, concept.getDescription());
		initialValues.put(DBHelper.TABLE_CONCEPT_COL_TERM, concept.getTerm().getId());
		initialValues.put(DBHelper.TABLE_CONCEPT_COL_DATE, concept.getDate().getTime());
		
		id = (int)db.insert(DBHelper.TABLE_CONCEPT_NAME, null, initialValues);
		
		concept.setId(id);
	}

	@Override
	public ArrayList<Course> getCourses() {
		ArrayList<Course> courses = new ArrayList<Course>();

		Cursor cursor = dbHelper.select(DBHelper.TABLE_COURSE_NAME);
		
		while (cursor.moveToNext()) {
			int id = cursor.getInt(cursor.getColumnIndex(DBHelper.TABLE_COURSE_COL_ID));
			String description = cursor.getString(cursor.getColumnIndex(DBHelper.TABLE_COURSE_COL_DESCRIPTION));
			Long date = cursor.getLong(cursor.getColumnIndex(DBHelper.TABLE_COURSE_COL_DATE));
			
			Course course = new Course(id, description, null, new Date(date));
			
			courses.add(course);
		}
		
		return courses;
	}

	@Override
	public Course getCourse(int courseId) {
		SQLiteDatabase db = dbHelper.getReadableDatabase();
		Course course = null;
		
		Cursor cursor = db.query( DBHelper.TABLE_COURSE_NAME, 
				new String[] { DBHelper.TABLE_COURSE_COL_DESCRIPTION, DBHelper.TABLE_COURSE_COL_DATE }, 
				DBHelper.TABLE_COURSE_COL_ID + " = ?", 
				new String [] { String.valueOf(courseId) }, 
				null, null, null );
		
		if (cursor.moveToFirst() ){
			String description = cursor.getString(cursor.getColumnIndex(DBHelper.TABLE_COURSE_COL_DESCRIPTION));
			Long date = cursor.getLong(cursor.getColumnIndex(DBHelper.TABLE_COURSE_COL_DATE));
			
			course = new Course(courseId, description, null, new Date(date));
		}
		
		getCategories(course);
		
		for (Category category : course.getCategories()){ 
			getTerms(category);
			for (Term term : category.getTerms()) 
				getConcepts(term);
		}
		
		return course;
	}

	@Override
	public void delete(Course course) {
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		
		db.delete(DBHelper.TABLE_COURSE_NAME, 
				DBHelper.TABLE_COURSE_COL_ID + " = ?", 
				new String [] {String.valueOf(course.getId())});
	}
	
	@Override
	public void delete(Category category) {
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		
		db.delete(DBHelper.TABLE_CATEGORY_NAME, 
				DBHelper.TABLE_CATEGORY_COL_ID + " = ?", 
				new String [] {String.valueOf(category.getId())});
	}

	@Override
	public void delete(Term term) {
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		
		db.delete(DBHelper.TABLE_TERM_NAME, 
				DBHelper.TABLE_TERM_COL_ID + " = ?", 
				new String [] {String.valueOf(term.getId())});
	}

	@Override
	public void delete(Concept concept) {
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		
		db.delete(DBHelper.TABLE_CONCEPT_NAME, 
				DBHelper.TABLE_CONCEPT_COL_ID + " = ?", 
				new String [] {String.valueOf(concept.getId())});
	}

	@Override
	public void update(Course course) {
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		ContentValues initialValues = new ContentValues();
		initialValues.put(DBHelper.TABLE_COURSE_COL_ID, course.getId());
		
		initialValues.put(DBHelper.TABLE_COURSE_COL_DESCRIPTION, course.getDescription());
		
		db.update(DBHelper.TABLE_COURSE_NAME, 
				initialValues, 
				DBHelper.TABLE_COURSE_COL_ID + " = ?", 
				new String[]{String.valueOf(course.getId())});
	}
	
	@Override
	public void update(Category category) {
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		ContentValues initialValues = new ContentValues();
		initialValues.put(DBHelper.TABLE_CATEGORY_COL_ID, category.getId());
		
		initialValues.put(DBHelper.TABLE_CATEGORY_COL_DESCRIPTION, category.getDescription());
		
		db.update(DBHelper.TABLE_CATEGORY_NAME, 
				initialValues, 
				DBHelper.TABLE_CATEGORY_COL_ID + " = ?", 
				new String[]{String.valueOf(category.getId())});
	}

	@Override
	public void update(Term term) {
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		ContentValues initialValues = new ContentValues();
		initialValues.put(DBHelper.TABLE_TERM_COL_ID, term.getId());
		
		initialValues.put(DBHelper.TABLE_TERM_COL_DESCRIPTION, term.getDescription());
		
		db.update(DBHelper.TABLE_TERM_NAME, 
				initialValues, 
				DBHelper.TABLE_TERM_COL_ID + " = ?", 
				new String[]{String.valueOf(term.getId())});
	}

	@Override
	public void update(Concept concept) {
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		ContentValues initialValues = new ContentValues();
		initialValues.put(DBHelper.TABLE_CONCEPT_COL_ID, concept.getId());
		
		initialValues.put(DBHelper.TABLE_CONCEPT_COL_DESCRIPTION, concept.getDescription());
		
		db.update(DBHelper.TABLE_CONCEPT_NAME, 
				initialValues, 
				DBHelper.TABLE_CONCEPT_COL_ID + " = ?", 
				new String[]{String.valueOf(concept.getId())});
	}
	
	// ================== Utility Methods ==========================
	
	private ArrayList<Category> getCategories(Course course){
		SQLiteDatabase db = dbHelper.getReadableDatabase();
		ArrayList<Category> categories = new ArrayList<Category>();
		
		Cursor cursor = db.query( DBHelper.TABLE_CATEGORY_NAME, 
				null, 
				DBHelper.TABLE_CATEGORY_COL_COURSE + " = ?", 
				new String [] { String.valueOf(course.getId()) }, 
				null, null, null );
		
		while (cursor.moveToNext()){
			int id = cursor.getInt(cursor.getColumnIndex(DBHelper.TABLE_CATEGORY_COL_ID));
			String description = cursor.getString(cursor.getColumnIndex(DBHelper.TABLE_CATEGORY_COL_DESCRIPTION));
			Long date = cursor.getLong(cursor.getColumnIndex(DBHelper.TABLE_CATEGORY_COL_DATE));
			
			Category category = new Category(id, course, description, null, new Date(date));
			categories.add(category);
		}
		
		course.setCategories(categories);
		
		return categories;
	}
	
	private ArrayList<Term> getTerms(Category category){
		SQLiteDatabase db = dbHelper.getReadableDatabase();
		ArrayList<Term> terms = new ArrayList<Term>();
		
		Cursor cursor = db.query( DBHelper.TABLE_TERM_NAME, 
				null, 
				DBHelper.TABLE_TERM_COL_CATEGORY + " = ?", 
				new String [] { String.valueOf(category.getId()) }, 
				null, null, null );
		
		while (cursor.moveToNext()){
			int id = cursor.getInt(cursor.getColumnIndex(DBHelper.TABLE_TERM_COL_ID));
			String description = cursor.getString(cursor.getColumnIndex(DBHelper.TABLE_TERM_COL_DESCRIPTION));
			Long date = cursor.getLong(cursor.getColumnIndex(DBHelper.TABLE_TERM_COL_DATE));
			
			Term term = new Term(id, category, description, null, new Date(date));
			terms.add(term);
		}
		
		category.setTerms(terms);
		
		return terms;
	}
	
	private ArrayList<Concept> getConcepts(Term term){
		SQLiteDatabase db = dbHelper.getReadableDatabase();
		ArrayList<Concept> concepts = new ArrayList<Concept>();
		
		Cursor cursor = db.query( DBHelper.TABLE_CONCEPT_NAME, 
				null, 
				DBHelper.TABLE_CONCEPT_COL_TERM + " = ?", 
				new String [] { String.valueOf(term.getId()) }, 
				null, null, null );
		
		while (cursor.moveToNext()){
			int id = cursor.getInt(cursor.getColumnIndex(DBHelper.TABLE_CONCEPT_COL_ID));
			String description = cursor.getString(cursor.getColumnIndex(DBHelper.TABLE_CONCEPT_COL_DESCRIPTION));
			Long date = cursor.getLong(cursor.getColumnIndex(DBHelper.TABLE_CONCEPT_COL_DATE));
			
			Concept concept = new Concept(id, term, description, new Date(date));
			concepts.add(concept);
		}
		
		term.setConcepts(concepts);
		
		return concepts;
	}
	
}
