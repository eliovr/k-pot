package com.helper.base;

import java.util.ArrayList;
import java.util.Stack;

import com.helper.base.R;
import com.helper.common.Category;
import com.helper.common.Concept;
import com.helper.common.Course;
import com.helper.common.IHelperView;
import com.helper.common.IController;
import com.helper.common.Item;
import com.helper.common.Term;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableLayout.LayoutParams;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

public class CourseActivity extends Activity implements IHelperView {

	private IController controller;
	
	AlertDialog newTermDialog;
	EditText term;
    EditText concept;
    AutoCompleteTextView category;
    TextView categoryLabel;
    
    AlertDialog searchDialog;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_course);
		initComponents();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_course, menu);
		return true;
	}
	
	protected void initComponents(){
		// Get selected course.
		int courseId = getIntent().getIntExtra("courseId", 0);
		controller = new Controller(this, this, courseId);
		this.setTitle("Course: " + controller.getCourse().getDescription());
		
		initNewTermDialog();
		initSearchDialog();
	    showRecent();
	}
	
	private void initNewTermDialog(){
		// Get attributes from the new_term_layout.
		LayoutInflater inflater = getLayoutInflater();
		LinearLayout newTermlayout = (LinearLayout) inflater.inflate(R.layout.new_term_layout, null);
		
	    term = (EditText)newTermlayout.findViewById(R.id.etTerm);
	    concept = (EditText)newTermlayout.findViewById(R.id.etConcept);
	    category = (AutoCompleteTextView)newTermlayout.findViewById(R.id.matvCategory);
	    categoryLabel = (TextView)newTermlayout.findViewById(R.id.tvCategory);
	    
	    categoryLabel.setOnClickListener(new TextView.OnClickListener() {
			@Override
			public void onClick(View v) {
				categoryLabel.setVisibility(View.GONE);
				category.setVisibility(View.VISIBLE);
				category.requestFocus();
			}
		});
	    
	    ArrayAdapter<Category> adapter = new ArrayAdapter<Category>(this, android.R.layout.simple_dropdown_item_1line, controller.getCourse().getCategories());
	    category.setAdapter(adapter);
	    
	    // Initialize the dialog for creating new terms. 
	    newTermDialog = new AlertDialog.Builder(this)
		    .setView(newTermlayout)
			.setPositiveButton(R.string.save, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int id) {
					controller.saveNewConcept();
				}
	       })
	       .setNegativeButton(R.string.string_cancel, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int id) {
					clearFields();
				}
	       })
	       .create();
	}
	
	private void initSearchDialog(){
		LayoutInflater inflater = getLayoutInflater();
		
	    final EditText input = (EditText) inflater.inflate(R.layout.input_dialog, null);
	    
	    searchDialog = new AlertDialog.Builder(this)
	    	.setView(input)
    		// Add action buttons
    		.setPositiveButton(R.string.search, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int id) {
					String search = input.getText().toString();
					controller.searchTerm(search);
				}
           })
           .setNegativeButton(R.string.string_cancel, null)
           .create();
	}
	
	// ===================== Event methods ========================
	public void showNewConceptLayout(View view){
	    newTermDialog.show();
    	newTermDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
    	term.requestFocus();
	}
	
	public void search(View view){
		searchDialog.show();
		searchDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
	}
	
	// ======================= Utility methods =========================
	
	/** Display only recently added terms. */
	private void showRecent(){
		TableLayout layout = (TableLayout)findViewById(R.id.itemsLayout);
		layout.removeAllViews();
		layout.setBackgroundColor(Color.WHITE);
		layout.setVisibility(View.VISIBLE);
		layout = termsTable(layout, controller.getRecentTerms(), true);
		
		TableRow row = newRowItem("Show all", Color.LTGRAY, true);
		row.setPadding(0, 25, 0, 25);
		row.setGravity(Gravity.CENTER);
		row.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				showAll();
			}
		});
		
		layout.addView(row);
	}
	
	/** Display all the categories, terms and concepts of the current course. */
	private void showAll(){
		TableLayout layout = (TableLayout)findViewById(R.id.itemsLayout);
		TextView txtTableTitle = (TextView)findViewById(R.id.txtTableTitle);
		
		layout.removeAllViews();
		layout.setBackgroundColor(Color.TRANSPARENT);
		layout = categoriesTable(layout, controller.getCourse().getCategories(), true, true);
		layout.setVisibility(View.VISIBLE);
		
		txtTableTitle.setText("All");
	}
	
	/** Display the given list of categories in the given table layout.
	 * @param layout Layout where the categories are to be displayed.
	 * @param categories List of categories to be displayed.
	 * @param showTerms True if the terms are to be displayed under each category.
	 * @param showConcepts True if the concepts are to be displayed under each term.
	 * */
	private TableLayout categoriesTable (TableLayout layout, ArrayList<Category> categories, boolean showTerms, boolean showConcepts){
		for (Category category : categories) {
			TableRow row = newRowItem(category.getDescription(), Color.GRAY, true);
			layout.addView(row);
			
			TableLayout termsLayout = null;
			
			if (showTerms){
				termsLayout = termsTable(newTableLayout(Color.WHITE, View.GONE), category.getTerms(), showConcepts);// newTableLayout(Color.LTGRAY, View.GONE);
				layout.addView(termsLayout);
				setOnClickEvent(row, termsLayout);
			}
			
			setLongClickEvent(layout, row, termsLayout, category);
		}
		
		return layout;
	}
	
	/** Display the given list of terms in the given table layout.
	 * @param layout Layout where the terms are to be displayed.
	 * @param terms List of terms to be displayed.
	 * @param showConcepts True if the concepts are to be displayed under each term.
	 * */
	private TableLayout termsTable (TableLayout layout, ArrayList<Term> terms, boolean showConcepts){
		for (Term term : terms) {
			TableRow row = newRowItem(term.getDescription(), Color.WHITE, false);
			layout.addView(row);
			
			TableLayout conceptsLayout = null;
			
			if (showConcepts){
				conceptsLayout = conceptsTable(newTableLayout(Color.WHITE, View.GONE), term.getConcepts());// newTableLayout(Color.WHITE, View.GONE);
				layout.addView(conceptsLayout);
				setOnClickEvent(row, conceptsLayout);
			}
			
			setLongClickEvent(layout, row, conceptsLayout, term);
		}
		
		return layout;
	}
	
	/** Display the given list of concepts in the given table layout.
	 * @param layout Layout where the concepts are to be displayed.
	 * @param concepts List of concepts to be displayed.
	 * */
	private TableLayout conceptsTable (TableLayout layout, ArrayList<Concept> concepts){
		for (Concept concept : concepts) {
			TableRow row = newRowItem(concept.getDescription(), Color.LTGRAY, false);
			layout.addView(row);
			setLongClickEvent(layout, row, null, concept);
		}
		
		return layout;
	}
	
	private TableLayout newTableLayout(int color, int visibility){
		TableLayout layout = new TableLayout(this);
		TableLayout.LayoutParams params = new TableLayout.LayoutParams();
		
		params.setMargins(5, 5, 5, 5);
		layout.setLayoutParams(params);
		layout.setBackgroundColor(color);
		layout.setVisibility(visibility);
		
		return layout;
	}
	
	private TableRow newRowItem(String description, int color, boolean clickable){
		TableRow row = new TableRow(this);
		TextView tv = new TextView(this);
		TableLayout.LayoutParams rowParams = new TableLayout.LayoutParams();
		
		rowParams.setMargins(2, 2, 2, 2);
		
		row.setLayoutParams(rowParams);
		row.setPadding(8, 15, 5, 15);
		row.setBackgroundColor(color);
		row.setClickable(clickable);
		row.addView(tv);
		
		tv.setText(description);
		tv.setTextSize(22);
		tv.setLayoutParams(new TableRow.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
		
		return row;
	}
	
	/** Sets an onClick event to the given "view".
	 * The event will switch the visibility of the "target" between VISIBLE and GONE 
	 * */
	private void setOnClickEvent(View view, final View target){
		
		view.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (target.getVisibility() == View.VISIBLE)
                	target.setVisibility(View.GONE);
                else
                	target.setVisibility(View.VISIBLE);
            }
        });
		
	}
	
	private void setLongClickEvent(final TableLayout parent, final TableRow row, final TableLayout associates, final Item item){
		
		row.setOnLongClickListener(new View.OnLongClickListener() {
			
			@Override
			public boolean onLongClick(View v) {
				AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
				LayoutInflater inflater = getLayoutInflater();
				
			    final EditText etInput = (EditText) inflater.inflate(R.layout.input_dialog, null);
			    etInput.setText(item.getDescription());
			    
			    builder.setView(etInput)
		    		// Add action buttons
		    		.setPositiveButton(R.string.string_save, new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int id) {
							TextView tv = (TextView)row.getChildAt(0);
							String desc = etInput.getText().toString();
							
							item.setDescription( desc );
							controller.update(item);
							tv.setText(desc);
							
							Toast.makeText(row.getContext(), "Saved", Toast.LENGTH_SHORT).show();
						}
		           })
		           .setNegativeButton(R.string.string_delete, new DialogInterface.OnClickListener() {
		   			
			   			@Override
			   			public void onClick(DialogInterface arg0, int arg1) {
			   				// TODO Auto-generated method stub
			   				controller.delete(item);
			   				parent.removeView(row);
			   				parent.removeView(associates);
			   				
			   				Toast.makeText(row.getContext(), "Deleted", Toast.LENGTH_SHORT).show();
			   			}
			   		})
		           .setNeutralButton(R.string.string_cancel, null);
			    
			    
			    builder.create().show();
			    
				return true;
			}
		});
	}
	
	/** Clear all input fields */
	private void clearFields() {
		term.setText("");
		concept.setText("");
		category.setText("");
	}
	
	// ======================= Interface methods =======================
	
	public Category getCategory() {
		String description = category.getText().toString();
		return new Category(description);
	}

	
	public Term getTerm() {
		String description = term.getText().toString();
		return new Term(description);
	}

	
	public Concept getConcept() {
		String description = concept.getText().toString();
		return new Concept(description);
	}
	
	@Override
	public Item getItem() {
		Category category = getCategory();
		Term term = getTerm();
		Concept concept = getConcept();
		
		term.setCategory(category);
		concept.setTerm(term);
		
		return concept;
	}

	@Override
	public void refresh() {
		clearFields();
		showRecent();
	}

	@Override
	public ArrayList<Course> getCourses() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void showSearchResult(final Stack<Term> terms) {
		if (terms.size() > 0){
			Term term = terms.pop();
			
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			LayoutInflater inflater = getLayoutInflater();
			ScrollView scrollView = new ScrollView(this);
			
			TableLayout conceptsLayout = conceptsTable((TableLayout) inflater.inflate(R.layout.term_layout, null), term.getConcepts());
			
			scrollView.addView(conceptsLayout);
			
		    builder
		    	.setView(scrollView)
		    	.setTitle(term.getDescription())
				.setNeutralButton(R.string.close, null)
			    .create();
		    
		    if (terms.size() > 0)
		    	builder.setPositiveButton("Next", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int id) {
						showSearchResult(terms);
					}
				});
		    
			builder.show();
		}
		else
			Toast.makeText(this, "Not found", Toast.LENGTH_LONG).show();
	}
	
	@Override
	public void toastMessage(String message){
		Toast.makeText(this, message, Toast.LENGTH_LONG).show();
	}

}
