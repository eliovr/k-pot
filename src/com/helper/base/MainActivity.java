package com.helper.base;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Stack;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableLayout.LayoutParams;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.helper.base.R;
import com.helper.common.*;

public class MainActivity extends Activity implements IHelperView{

	IController controller;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		initComponents();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}
	
	protected void initComponents(){
		controller = new Controller(this, this);
		showCourses();
	}
	
	// ================== Event methods ========================
	
	public void editCourses(View view){
		LinearLayout cancelSaveLayout = (LinearLayout)findViewById(R.id.cancelSaveLayout);
		LinearLayout newCourseLayout = (LinearLayout)findViewById(R.id.newCourseLayout);
		TableLayout coursesLayout = (TableLayout)findViewById(R.id.coursesLayout);
		
		cancelSaveLayout.setVisibility(View.VISIBLE);
		newCourseLayout.setVisibility(View.GONE);
		coursesLayout.setBackgroundColor(Color.WHITE);
		
		showEditableCourses();
	}
	
	public void cancelEditCourses(View view){
		refresh();
	}
	
	public void cancelNewCourse(View view){
		LinearLayout newCourseLayout = (LinearLayout)findViewById(R.id.newCourseLayout);
		newCourseLayout.setVisibility(View.GONE);
	}
	
	public void createNewCourse(View view){
		LinearLayout newCourseLayout = (LinearLayout)findViewById(R.id.newCourseLayout);
		TextView etCourse = (TextView)findViewById(R.id.etCourse);
		
		newCourseLayout.setVisibility(View.VISIBLE);
		etCourse.requestFocus();
	}
	
	/** Save all courses */
	public void saveCourses(View view){
		controller.saveCourses();
	}
	
	/** Save new Course */
	public void saveCourse(View view){
		controller.saveNewCourse();
	}
	
	// ==================== Utility methods ======================
	
	/** Display the courses all the courses - READ ONLY */
	private void showCourses(){
		LinearLayout coursesLayout = (LinearLayout)findViewById(R.id.coursesLayout);
		TextView tvSample = (TextView)findViewById(R.id.textViewSample);
		int color = Color.LTGRAY;
		
		coursesLayout.setBackgroundColor(Color.TRANSPARENT);
		coursesLayout.removeAllViews();
		
		for (Course course : controller.getCourses()) {
			LayoutParams params = new LayoutParams();
			params.setMargins(15, 15, 15, 0);

			TextView tv = new TextView(this);
			tv.setLayoutParams(params);
			tv.setHeight(120);
			tv.setTextSize(tvSample.getTextSize());
			tv.setGravity(tvSample.getGravity());
			tv.setText(course.getDescription());
			
			if (color == Color.LTGRAY)
				color = Color.WHITE;
			else
				color = Color.LTGRAY;
			tv.setBackgroundColor(color);
			
			final int courseId = course.getId();
			
			tv.setOnClickListener(new View.OnClickListener() {
	            public void onClick(View v) {
	                Intent myIntent = new Intent(v.getContext(), CourseActivity.class);
	                myIntent.putExtra("courseId", courseId);
	                startActivity(myIntent);
	            }
	        });
			
			coursesLayout.addView(tv);
		}
	}
	
	/** Display the courses all the courses - READ/WRITE */
	private void showEditableCourses(){
		TableLayout coursesLayout = (TableLayout)findViewById(R.id.coursesLayout);
		TextView tvSample = (TextView)findViewById(R.id.textViewSample);
		
		coursesLayout.removeAllViews();
		
		for (Course course : controller.getCourses()) {
			TableRow tr = new TableRow(this);
			ImageButton ib = new ImageButton(this);
			EditText et = new EditText(this);
			
			ib.setImageResource(R.drawable.cancel);
			ib.setBackgroundColor(Color.TRANSPARENT);
			ib.setOnClickListener(new View.OnClickListener() {
	            public void onClick(View v) {
	            	TableLayout coursesLayout = (TableLayout)findViewById(R.id.coursesLayout);
	            	coursesLayout.removeView((View)v.getParent());
	            }
	        });
			
			et.setWidth(coursesLayout.getWidth() - ib.getWidth());
			et.setHeight(120);
			et.setTextSize(tvSample.getTextSize());
			et.setText(course.getDescription());
			et.setId(course.getId());
			
			tr.addView(ib);
			tr.addView(et);
			coursesLayout.addView(tr);
		}
	}
	
	
	//===================== Interface methods ====================
	/*
	@Override
	public Course getCourse() {
		EditText etCourse = (EditText)findViewById(R.id.etCourse);
		String description = etCourse.getText().toString(); 
		return new Course(description);
	}

	@Override
	public Category getCategory() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Term getTerm() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Concept getConcept() {
		// TODO Auto-generated method stub
		return null;
	}
	*/
	
	@Override
	public Item getItem() {
		EditText etCourse = (EditText)findViewById(R.id.etCourse);
		String description = etCourse.getText().toString(); 
		return new Course(description);
	}

	@Override
	public void refresh() {
		TextView etCourse = (EditText)findViewById(R.id.etCourse);
		LinearLayout cancelSaveLayout = (LinearLayout)findViewById(R.id.cancelSaveLayout);
		LinearLayout newCourseLayout = (LinearLayout)findViewById(R.id.newCourseLayout);
		
		cancelSaveLayout.setVisibility(View.GONE);
		newCourseLayout.setVisibility(View.GONE);
		
		etCourse.setText("");
		showCourses();
	}

	@Override
	public ArrayList<Course> getCourses() {
		TableLayout coursesLayout = (TableLayout)findViewById(R.id.coursesLayout);
		ArrayList<Course> courses = new ArrayList<Course>();
		
		for (int i = 0 ; i < coursesLayout.getChildCount() ; i++) {
			TableRow tr = (TableRow)coursesLayout.getChildAt(i);
			EditText et = (EditText)tr.getChildAt(1);
			String description = et.getText().toString();
			int id = et.getId();
			
			courses.add(new Course(id, description, null, Calendar.getInstance().getTime()));
		}
		
		return courses;
	}

	@Override
	public void showSearchResult(Stack<Term> term) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void toastMessage(String message) {
		Toast.makeText(this, message, Toast.LENGTH_LONG).show();
	}

}
