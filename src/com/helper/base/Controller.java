package com.helper.base;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Stack;

import android.content.Context;

import com.helper.common.*;

public class Controller implements IController{

	private IHelperView view;
	private IModel model;
	private Course course;
	
	public Controller (Context context, IHelperView view){
		this.model = new Model(context);
		this.view = view;
		this.course = null;
	}
	
	public Controller (Context context, IHelperView view, int courseId){
		this.model = new Model(context);
		this.view = view;
		this.course = model.getCourse(courseId);
	}
	
	@Override
	public void saveNewConcept() {
		Concept concept = (Concept)view.getItem();
		concept.getTerm().getCategory().setCourse(course);
		
		model.insert(concept.getTerm().getCategory());
		model.insert(concept.getTerm());
		
		String[] concepts = concept.getDescription().split(";");
		for (String con : concepts) {
			concept.setDescription(con.trim());
			model.insert(concept);
		}
		
		
		// Refresh all the values of the course.
		course = model.getCourse(course.getId());
		
		view.refresh();
		view.toastMessage("Saved");
	}

	@Override
	public ArrayList<Course> getCourses() {
		return model.getCourses();
	}

	@Override
	public void saveNewCourse() {
		Course course = (Course) view.getItem();
		
		model.insert(course);
		
		view.refresh();
		view.toastMessage("Saved");
	}

	@Override
	public Course getCourse() {
		return course;
	}
	
	@Override
	public ArrayList<Term> getRecentTerms(){
		ArrayList<Term> terms = new ArrayList<Term>();
		Calendar oldest = Calendar.getInstance();
		oldest.add(Calendar.DAY_OF_YEAR, -14);
		
		for (Category cat : course.getCategories()) {
			for (Term term : cat.getTerms()) {
				if ( oldest.getTime().before(term.getDate()) ){
					terms.add(term);
				}
			}
		}
		
		return terms;
	}

	@Override
	public void saveCourses() {
		ArrayList<Course> oldCourses = getCourses();
		ArrayList<Course> newCourses = view.getCourses();
		boolean exists;
		
		for (Course oldCourse : oldCourses) {
			exists = false;
			for (Course newCourse : newCourses) {
				if (oldCourse.getId() == newCourse.getId()){
					exists = true;
					if ( !oldCourse.getDescription().equalsIgnoreCase(newCourse.getDescription()) )
						model.update(newCourse);
					
					break;
				}
			}
			
			if (!exists)
				model.delete(oldCourse);
		}
		
		view.refresh();
	}

	@Override
	public void update(Item item) {
		if (item instanceof Category){
			model.update((Category)item);
		}
		else if (item instanceof Term){
			model.update((Term)item);
		}
		else if (item instanceof Concept){
			model.update((Concept)item);
		}
		
		view.toastMessage("Saved");
	}

	@Override
	public void delete(Item item) {
		if (item instanceof Category){
			Category category = (Category)item;
			course.getCategories().remove(item);
			model.delete(category);
		}
		else if (item instanceof Term){
			Term term = (Term)item;
			term.getCategory().getTerms().remove(item);
			model.delete(term);
		}
		else if (item instanceof Concept){
			Concept concept = (Concept)item;
			concept.getTerm().getConcepts().remove(item);
			model.delete(concept);
		}
		
		view.toastMessage("Deleted");
	}

	@Override
	public void searchTerm(String search) {
		Stack<Term> terms = new Stack<Term>();
		Term oTerm = null;
		
		for (Category cat : course.getCategories()) {
			for (Term term : cat.getTerms()) {
				if(term.getDescription().toLowerCase().contains(search))
					oTerm = term;
				else
					for (Concept concept : term.getConcepts())
						if(concept.getDescription().toLowerCase().contains(search))
							oTerm = term;
					
				if (oTerm != null){
					terms.push(oTerm);
					oTerm = null;
				}
			}
		}
		
		view.showSearchResult(terms);
	}

}
