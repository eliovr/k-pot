package com.helper.common;

import java.util.ArrayList;
import java.util.Date;

public class Term extends Item{

	private Category category;
	
	private ArrayList<Concept> concepts;
	
	public Term (String description){
		super(description);
		this.category = null;
		this.concepts = new ArrayList<Concept>();
	}
	
	public Term (int id, Category category, String description, ArrayList<Concept> concepts, Date date){
		super(id, description, date);
		this.category = category;
		this.concepts = concepts;
	}
	
	public void addConcept(Concept concept){
		if (concepts == null)
			concepts = new ArrayList<Concept>();
		
		concept.setTerm(this);
		concepts.add(concept);
	}

	/**
	 * @return the category
	 */
	public Category getCategory() {
		return category;
	}
	/**
	 * @param category the category to set
	 */
	public void setCategory(Category category) {
		this.category = category;
	}
	
	/**
	 * @return the concept
	 */
	public ArrayList<Concept> getConcepts() {
		return concepts;
	}
	/**
	 * @param concept the concept to set
	 */
	public void setConcepts(ArrayList<Concept> concepts) {
		this.concepts = concepts;
	}
}
