package com.helper.common;

import java.util.Calendar;
import java.util.Date;

public abstract class Item {

	protected int id;
	
	protected String description;
	
	protected Date date;
	
	public Item(String description){
		this.description = description;
		this.id = 0;
		this.date = Calendar.getInstance().getTime();
	}
	
	public Item(int id, String description, Date date){
		this.description = description;
		this.id = id;
		this.date = date;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	
	public String toString(){
		return this.description;
	}
}
