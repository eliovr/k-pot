package com.helper.common;

import java.util.ArrayList;
import java.util.Stack;

public interface IHelperView {
	
	public ArrayList<Course> getCourses();
	
	public void refresh();
	
	public void showSearchResult(Stack<Term> terms);
	
	public Item getItem();
	
	public void toastMessage(String message);

}
