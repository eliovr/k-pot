package com.helper.common;

import java.util.ArrayList;

public interface IModel {
	
	public void insert(Course course);
	
	public void insert(Category category);
	
	public void insert(Concept concept);
	
	public void insert(Term term);
	
	public void delete(Course course);
	
	public void delete(Category category);
	
	public void delete(Term term);
	
	public void delete(Concept concept);
	
	public void update(Course course);
	
	public void update(Category category);
	
	public void update(Term term);
	
	public void update(Concept concept);
	
	public ArrayList<Course> getCourses();
	
	public Course getCourse(int id);
	
}
