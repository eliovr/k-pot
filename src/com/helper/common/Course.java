package com.helper.common;

import java.util.Date;
import java.util.ArrayList;

public class Course extends Item{
	
	private ArrayList<Category> categories;
	
	public Course(String description){
		super(description);
		this.categories = new ArrayList<Category>();
	}
	
	public Course(int id, String description, ArrayList<Category> categories, Date date){
		super(id, description, date);
		this.categories = categories;
	}

	public void addCategory(Category category){
		if (categories == null)
			categories = new ArrayList<Category>();
		
		category.setCourse(this);
		categories.add(category);
	}

	public ArrayList<Category> getCategories() {
		return categories;
	}

	public void setCategories(ArrayList<Category> categories) {
		this.categories = categories;
	}
}
