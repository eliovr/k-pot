package com.helper.common;

import java.util.Date;

public class Concept extends Item{
	
	private Term term;
	
	public Concept(String description){
		super(description);
		this.term = null;
	}
	
	public Concept(int id, Term term, String description, Date date){
		super(id, description, date);
		this.term = term;
	}

	public Term getTerm() {
		return term;
	}

	public void setTerm(Term term) {
		this.term = term;
	}

}
