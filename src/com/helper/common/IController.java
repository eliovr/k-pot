package com.helper.common;

import java.util.ArrayList;

public interface IController {
	
	public ArrayList<Course> getCourses();
	
	public void saveNewCourse();
	
	public void saveNewConcept();
	
	public void saveCourses();
	
	public Course getCourse();
	
	public void update(Item item);
	
	public void delete(Item object);
	
	public ArrayList<Term> getRecentTerms();
	
	public void searchTerm(String search);
}
