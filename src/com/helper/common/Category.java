package com.helper.common;

import java.util.ArrayList;
import java.util.Date;

public class Category extends Item{
	
	public static final String UNDEFINED_CATEGORY = "undefined";
	
	private Course course;
	
	private ArrayList<Term> terms;
	
	public Category(String description){
		super(description);
		this.course = null;
		this.terms = new ArrayList<Term>();
		setDescription(description);
	}
	
	public Category(int id, Course course, String description, ArrayList<Term> terms, Date date){
		super(id, description, date);
		this.course = course;
		this.terms = terms;
	}

	public void addTerm(Term term){
		if (terms == null)
			terms = new ArrayList<Term>();
		
		term.setCategory(this);
		terms.add(term);
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public void setDescription(String description) {
		if (description.equalsIgnoreCase(""))
			this.description = UNDEFINED_CATEGORY;
		else
			this.description = description;
	}

	public ArrayList<Term> getTerms() {
		return terms;
	}

	public void setTerms(ArrayList<Term> terms) {
		this.terms = terms;
	}
}
