package com.helper.db;

//import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

public class DBHelper extends SQLiteOpenHelper {

	public static final int DB_VERSION = 1;
    public static final String DB_SCHEMA = "MyFirstApp.db";
    
 // ------------------------- TABLE "COURSE" -------------------------------
    public static final String TABLE_COURSE_NAME = "`" + DB_SCHEMA + ".course`";
    public static final String TABLE_COURSE_COL_ID = BaseColumns._ID;
    public static final String TABLE_COURSE_COL_DESCRIPTION = "description";
    public static final String TABLE_COURSE_COL_DATE = "date";
    
    private static final String DROP_TABLE_COURSE = "DROP TABLE IF EXISTS " + TABLE_COURSE_NAME;
    private static final String CREATE_TABLE_COURSE = 
    		"CREATE TABLE IF NOT EXISTS " + TABLE_COURSE_NAME + " (" +
    				TABLE_COURSE_COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
    				TABLE_COURSE_COL_DESCRIPTION + " TEXT, " +
    				TABLE_COURSE_COL_DATE + " INTEGER" +
    		")";
    
    // ------------------------- TABLE "CATEGORY" -------------------------------
    public static final String TABLE_CATEGORY_NAME = "`" + DB_SCHEMA + ".category`";
    public static final String TABLE_CATEGORY_COL_ID = BaseColumns._ID;
    public static final String TABLE_CATEGORY_COL_DESCRIPTION = "description";
    public static final String TABLE_CATEGORY_COL_COURSE = "course";
    public static final String TABLE_CATEGORY_COL_DATE = "date";
    
    private static final String DROP_TABLE_CATEGORY = "DROP TABLE IF EXISTS " + TABLE_CATEGORY_NAME;
    private static final String CREATE_TABLE_CATEGORY = 
    		"CREATE TABLE IF NOT EXISTS " + TABLE_CATEGORY_NAME + " (" +
    				TABLE_CATEGORY_COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
    				TABLE_CATEGORY_COL_DESCRIPTION + " TEXT, " +
    				TABLE_CATEGORY_COL_DATE + " INTEGER, " +
    				TABLE_CATEGORY_COL_COURSE + " INTEGER REFERENCES "+ TABLE_COURSE_NAME +"("+ TABLE_COURSE_COL_ID +") ON DELETE CASCADE" +
    		")";
    
    // ------------------------- TABLE "TERM" -------------------------------
    public static final String TABLE_TERM_NAME = "`" + DB_SCHEMA + ".term`";
    public static final String TABLE_TERM_COL_ID = BaseColumns._ID;
    public static final String TABLE_TERM_COL_DESCRIPTION = "description";
    public static final String TABLE_TERM_COL_CATEGORY = "category";
    public static final String TABLE_TERM_COL_DATE = "date";
    
    private static final String DROP_TABLE_TERM = "DROP TABLE IF EXISTS " + TABLE_TERM_NAME;
    private static final String CREATE_TABLE_TERM = 
    		"CREATE TABLE IF NOT EXISTS " + TABLE_TERM_NAME + " (" +
    				TABLE_TERM_COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
    				TABLE_TERM_COL_DESCRIPTION + " TEXT, " +
    				TABLE_TERM_COL_DATE + " INTEGER, " +
    				TABLE_TERM_COL_CATEGORY + " INTEGER REFERENCES "+ TABLE_CATEGORY_NAME +"("+ TABLE_CATEGORY_COL_ID +") ON DELETE CASCADE" +
    		")";
	
    // ---------------------------- TABLE "CONCEPT" -------------------------
    public static final String TABLE_CONCEPT_NAME = "`" + DB_SCHEMA + ".concept`";
    public static final String TABLE_CONCEPT_COL_ID = BaseColumns._ID;
    public static final String TABLE_CONCEPT_COL_DESCRIPTION = "description";
    public static final String TABLE_CONCEPT_COL_TERM = "term";
    public static final String TABLE_CONCEPT_COL_DATE = "date";
    
    private static final String DROP_TABLE_CONCEPT = "DROP TABLE IF EXISTS " + TABLE_CONCEPT_NAME;
    private static final String CREATE_TABLE_CONCEPT = 
    		"CREATE TABLE IF NOT EXISTS " + TABLE_CONCEPT_NAME + " (" +
    				TABLE_CONCEPT_COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
    				TABLE_CONCEPT_COL_DESCRIPTION + " TEXT, " +
    				TABLE_CONCEPT_COL_DATE + " INTEGER, " +
    				TABLE_CONCEPT_COL_TERM + " INTEGER REFERENCES "+ TABLE_TERM_NAME +"("+ TABLE_TERM_COL_ID +") ON DELETE CASCADE" +
    		")";
    
    // ---------------------------- MASTER VALUES ------------------------
    
    public static final String [] COURSES = {"Swedish", "Greek", "Spanish"};
    
    // =====================================================================
	
    public DBHelper(Context context) {
		super(context, DB_SCHEMA, null, DB_VERSION);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(DROP_TABLE_COURSE);
		db.execSQL(DROP_TABLE_CATEGORY);
		db.execSQL(DROP_TABLE_TERM);
		db.execSQL(DROP_TABLE_CONCEPT);
		
		db.execSQL(CREATE_TABLE_COURSE);
		db.execSQL(CREATE_TABLE_CATEGORY);
		db.execSQL(CREATE_TABLE_TERM);
		db.execSQL(CREATE_TABLE_CONCEPT);
		
		//populate(db);
	}

	@Override
	public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}
	
	/*
	private void populate(SQLiteDatabase db) {
		ContentValues cv = null;
		
		for (String cat : COURSES){
			cv = new ContentValues();
			cv.put(TABLE_COURSE_COL_DESCRIPTION, cat);
			db.insert(TABLE_COURSE_NAME, null, cv);
		}
	}
	*/
	
	/** Get all the values from a table */
	public Cursor select(String table){
		SQLiteDatabase db = getReadableDatabase();
		String query = "SELECT * FROM " + table;
		return db.rawQuery(query, null);
	}
	
	public Cursor selectAll(){
		SQLiteDatabase db = getWritableDatabase();
		
		String query = "SELECT " +
					"cat." + TABLE_CATEGORY_COL_DESCRIPTION +
					", t." + TABLE_TERM_COL_DESCRIPTION +
					", c." + TABLE_CONCEPT_COL_DESCRIPTION +
				" FROM " + 
					TABLE_TERM_NAME + " t " +
				" JOIN " + TABLE_CONCEPT_NAME + " c " +
					"ON c." + TABLE_CONCEPT_COL_TERM  + " = t." + TABLE_TERM_COL_ID +
				" LEFT JOIN " + TABLE_CATEGORY_NAME + " cat " +
					"ON t." + TABLE_TERM_COL_CATEGORY + " = cat." + TABLE_CATEGORY_COL_ID;
		
		return db.rawQuery(query, null);
	}
}
